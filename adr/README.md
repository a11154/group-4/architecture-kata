# Architecture Kata (ADR)

## Empezando

Este repositorio contiene las ADRs de la Kata del grupo 4.

## Integrantes 
- Mauricio Romero
- Edwin Gallardo
- Freddy Tacuri
- Héctor Andrade

## Estándar de registro

- Dentro de la carpeta ADR se debe crear para cada nueva ADR una carpeta con el formato de nombre adr-[nombre-de-la-adr] (omitir los corchetes).

- El detalle de la ADR debe ser colocado dentro de un archivo Index.md dentro de su respectiva carpeta.

- El formato del contenido Index.md se encuentra en el archivo [example.md](https://gitlab.com/a11154/group-4/architecture-kata/-/blob/dev/example.md)


## Índice
- [ADR Arquitectura](arquitectura/Index.md)
- [ADR Base de datos](base-de-datos/Index.md)
- [ADR Consumo de servicio de plagio](consumo-servicio-de-plagio/Index.md)
- [ADR Integración de base de datos](integracion-base-de-datos/Index.md)
- [ADR Middleware](middleware/Index.md)
- [ADR Sincronización de datos](sincronizacion-de-datos/index.md)
