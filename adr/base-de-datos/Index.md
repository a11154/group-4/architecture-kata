# Título

Base de datos

## Estado

Aceptado

## Contexto

Para el manejo de la estructura de los datos como tal, como estudiante con tareas, y tareas con calificación de tareas, etc., se requiere usar una base de datos relacional, además también para no afectar la integridad del LMS y ajustarse al presupuesto.

## Decisión

- [X] Base de datos PostgreSQL

## Consecuencias

Instalar una instancia del servidor postgres.

## Notas

Fecha de creación: 2022-05-20