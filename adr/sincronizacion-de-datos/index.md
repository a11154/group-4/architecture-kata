# Título

Sincronización de Datos

## Estado

Definido

## Contexto

Se requiere almacenar en la BDD del aplicativo información de docentes, alumnos y asignaturas.

## Decisión

Proceso batch que realice la sincronización de información de la información en el LMS y el aplicativo de califcación de tareas.

## Consecuencias

Crear un proceso que se conecte a la capa middleware para obtener la información del LMS.

## Notas

Fecha creación: 2022-05-21