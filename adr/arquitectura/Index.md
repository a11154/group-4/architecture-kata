# Título

Arquitectura del sistema

## Estado

Definido

## Contexto

Mantener un equilibrio entre lo económico y funcional.

## Decisión

Monolito modular

## Consecuencias

Implementación de la arquitectura, pero mantener el aislamiento entre componentes para facilitar una posible migración a otra arquitectura si es necesario.

Se recomienda una evaluación de la arquitectura a futuro para evoluacionar en el momento adecuado si las condiciones lo requieren.

## Notas

Fecha creación: 2022-05-20

Ventajas:
- Fácil mantenimiento.
- Fácil despliegue.

Desventajas:
- Se debe revisar periodicamente las implementaciones para garantizar una posible migración con el menor impacto posible.