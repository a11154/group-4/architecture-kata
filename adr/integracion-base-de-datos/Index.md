# Título

Interacción base de datos

## Estado

Aceptado

## Contexto

Se requiere almacenar las tareas (código) de los estudiantes, calificaciones y almacenamiento de históricos para auditoria. Los componente encargados de cada una de estas funciones deben interactuar con la base de datos.

## Decisión

- [X] Integración de ORM

## Consecuencias

Integración de ORM en el proyecto compatible con PostgreSQL.

## Notas

Fecha creación: 2022-05-20