# Título

Consumo de servicio de plagio

## Estado

Definido

## Contexto

Comprobar si los estudiantes han realizado plagio con sus archivos cargados

## Decisión

Consumir los servicios de verificación de plagio por medio de un adaptador e integrar a  Turnitin como proveedor


## Consecuencias

Implementación de adaptador para integración de proveedores de verificación de plagio
