# Título

Middleware

## Estado

Definido

## Contexto

Se requiere la integración con el sistema LMS.

## Decisión

Crear un componente middleware que permita la conexión al mainframe para el proceso de login y sincronización de datos.

## Consecuencias

Se debe desarrollar un componente que consuma los servicios expuestos por el LMS
Realizar el mapeo entre el LMS y el Sistema de calificaciones

## Notas

Fecha creación: 2022-05-20.