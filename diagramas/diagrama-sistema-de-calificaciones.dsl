workspace {

    model {
        professorPerson = person "docente" "Crea y configura las tareas"
        studentPerson = person "estudiante" "Sube sus tareas"
        adminPerson = person "administrador" "Genera reportes y administra el sistema"
        softwareSystemCalifications = softwareSystem "Sistema de calificaciones" {
            webApp = container "Aplicación Web" "Interfaz web para la gestión de las tareas" {
                loginModule = component "Login Module" "Permite la gestión de autenticación"
                taskModule = component "Task Module" "Permite la gestión de tareas"
                reportModule = component "Report Module" "Permite la gestión de reportes"
                adminModule = component "Admin Module" "Permite la gestión de parámetros y configuración general del sistema"
            }
            webApi = container "Application Api" "Provee la funcionalidad al aplicativo a través de servicios web" {
                
                loginController = component "Sign In Controller" "Funcionalidad para la autenticación de usuarios" "Spring MVC Rest Controller"
                taskController = component "Task Controller" "Funcionalidad para la gestión de las tareas" "Spring MVC Rest Controller"
                reportController = component "Report Controller" "Funcionalidad para la generación de reportes para auditoria" "Spring MVC Rest Controller"
                gradeController = component "Grade Controller" "Funcionalidad para la calificación del código fuente." "Spring MVC Rest Controller"
                antiPlagiarismController = component "Anti Plagiarism Controller" "Funcionalidad para la verificación anti plagio del código fuente." "Spring MVC Rest Controller"
                adminController = component "Admin Controller" "Funcionalidad para la parametrización del sistema" "Spring MVC Rest Controller"
                
            }
            syncData = container "Sincronización de datos" "Proceso batch para sincronizar la información académica" {
                syncController = component "Sync Batch" "Actualiza la información del LMS dentro del sistema"
            }
            middleware = container "Middleware" "Permite la comunicación a través de conversiones de tramas hacia el LMS" {
                loginControllerMiddleware = component "Sign In Controller Middleware" "Transforma las peticiones de LogIn" "Spring MVC Rest Controller"
                syncControllerMiddleware = component "Sync Controller Middleware" "Transforma las peticiones de Sincronización" "Spring MVC Rest Controller"
            }
            orm = container "Object Relational Mapping" "Gestiona el acceso a la base de datos" {
            }
            dataBase = container "Base de Datos" {
            description "Almacena información del sistema" 
            tags "DataBase" 
            }
        }
        
        softwareSystemLMS = softwareSystem "LMS" "Sistema de manejo de aprendizaje de la universidad" "External"{
            lms = container "Aplicación Web"{
            }
        }
        turnitin = softwareSystem "Turnitin" "Servicio Web externo para verificación antiplagio" "External" {
            webServiceTurnitin = container "Aplicación Web Externo"{
             }
        }
        
        ### RELACIONES ###
        
        #Relación: Usuarios > Sistema de calificaciones
        professorPerson -> softwareSystemCalifications "Crear y configura tareas"
        studentPerson -> softwareSystemCalifications "Subir tareas con código fuente"
        adminPerson -> softwareSystemCalifications "Generar reportes y administra sistema"
        
        #Relación: Usuarios > Módulos Web App
        professorPerson -> loginModule "Solicita autentificación"
        professorPerson -> taskModule "Accede a administración de tareas"
        studentPerson -> loginModule "Solicita autentificación"
        studentPerson -> taskModule "Accede a cumplimiento de tareas"
        adminPerson -> loginModule "Solicita autentificación"
        adminPerson -> adminModule "Accede a administración del sistema"
        adminPerson -> reportModule "Accede a reportes de auditoría"

        #Relación: Contenedores
        webApi -> orm "Usa"
        syncData -> orm "Usa"
        orm -> dataBase "Gestiona conexión y peticiones"
        middleware -> lms "Solicita información académica y de autenticación"
        webApi -> webServiceTurnitin "Envia código fuente para revisión antiplagio"
        
        #Relación: Componentes WebApp > Componentes WebApi
        loginModule -> loginController "Consume"
        adminModule -> adminController "Consume"
        taskModule -> taskController "Consume"
        reportModule -> reportController "Consume"
        
        #Relación: Componentes WebApi > Componentes WebApi
        taskController -> gradeController "Consume"
        gradeController -> antiPlagiarismController "Consume"
        
        #Relación: Componentes WebApi > Componentes Middleware
        syncController -> syncControllerMiddleware "Consume"
        loginController -> loginControllerMiddleware "Consume"
        
        #Relación: Componentes WebApi > ORM
        taskController -> orm "Envía peticiones de base de datos"
        reportController -> orm "Envía peticiones de base de datos"
        gradeController -> orm "Envía peticiones de base de datos"
        adminController -> orm "Envía peticiones de base de datos"
        
        #Relación: Componentes WebApi > Componentes externos
        antiPlagiarismController -> webServiceTurnitin "Consume"
        
        #Relación: Componentes Middleware > Sistema LMS
        loginControllerMiddleware -> softwareSystemLMS "Consume"
        syncControllerMiddleware -> softwareSystemLMS "Consume"
    }

    views {
        systemContext softwareSystemCalifications {
            include *
            autolayout lr
        }

        container softwareSystemCalifications {
            include *
            autolayout lr
        }
        
        component webApp {
            include *
            autolayout
        }
        
        component webApi {
            include *
            autolayout
        }
        
        component middleware {
            include *
            autolayout lr
        }
        
        theme default
        
        styles {
        element "External" {
            background #999999
            color #ffffff
            }
        element "DataBase" {
            shape Cylinder
            }
        }
    }

}