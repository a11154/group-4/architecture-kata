# Architecture Kata GRUPO 4 - Diagramas

## Diagrama de contexto

![Diagrama de contexto](images/structurizr-1-Sistemadecalificaciones-SystemContext.png "Diagrama de contexto")

## Diagrama de contenedores

![Diagrama de contenedores](images/structurizr-1-Sistemadecalificaciones-Container.png "Diagrama de contenedores")

## Diagrama de componentes - Middleware

![Diagrama de componentes - Middleware](images/structurizr-1-Sistemadecalificaciones-Middleware-Component.png "Diagrama de componentes - Middleware")

## Diagrama de componentes - Aplicación Web

![Diagrama de componentes - WebApp](images/structurizr-1-Sistemadecalificaciones-AplicacinWeb-Component.png "Diagrama de componentes - WebApp")

## Diagrama de componentes - WebApi

![Diagrama de componentes - WebApi](images/structurizr-1-Sistemadecalificaciones-ApplicationApi-Component.png "Diagrama de componentes - WebApi")
